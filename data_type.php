<?php
    //BOOLEAN EXAMPLE START HERE
    $decision = true;
    if($decision){
        echo "The decision is True!<br>";
    }

    $decision = false;
    if($decision){
        echo "The decision is False!<br>";
    }
    //boolean example ends here

//Integer example start here
    $value1 = 100;      //Integer
    $value2 = 55.35;    //Float


//Integer example ends here

//String example start here

    $myString1 = 'abcd1234#$% $value1';
    $myString2 = "abcd1234#$% $value1";

    echo $myString1."<br>";

    echo $myString2."<br>";

    $heredocString = <<<BITM
    heredoc line1 $value1 <br>
    heredoc line2 $value1 <br>
    heredoc line3 $value1 <br>
    heredoc line4 $value1 <br>
BITM;

    echo $heredocString;

    $nowdocString = <<<'BITM'

    nowdoc line1 $value1 <br>
    nowdoc line2 $value1 <br>
    nowdoc line3 $value1 <br>
BITM;

    echo $nowdocString;

//String example ends here

//Array example starts here

    $indexedArray = array(1, 2, 3, '11'=>4, 5, 6, 7, );

    print_r($indexedArray). "<br>" ;

    $indexedArray = array("Toyota", "BMW", 3, 5, 7, "Jaguar", "Nissan", "Ford");

    print_r($indexedArray);

    $ageArray = array("Rahim"=>23, "Moynar Ma"=>57, "Kuddus"=>35, "Abul"=>36);

    print_r($ageArray);

    $ageArray['Moynar Ma'] = 34;
    echo $ageArray['Moynar Ma'];

    echo "<pre>";

    print_r($ageArray);
    echo "</pre>";
//Array example ends here
?>