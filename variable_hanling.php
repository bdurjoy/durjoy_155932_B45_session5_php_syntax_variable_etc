<?php
    $var = 'The122.34343The';
    $float_value_of_var = floatval($var);


    echo $float_value_of_var."<br>"; // 122.34343

    if(empty($var)){
        echo '$var is empty<br>';
    }else{
        echo '$var is not empty<br>';
    }

    $var = array( 1, 2, 3, 4, 5, 6);
    if(is_array($var)){
        echo '$var is an array<br>';
    }else{
        echo '$var is not an array<br>';
    }

    $strSerialize = serialize($var);

    echo $strSerialize."<br>";

    $newVar = unserialize($strSerialize);

    print_r($newVar);

    echo "<br>";
    echo "<br>";

    var_dump($var);
    echo "<br>";
    echo "<br>";

    var_export($var);

    echo gettype($var);
    echo "<br>";


    unset($var);
    print_r($var);

    $a = false;
    $b = 0;

    // Since $a is a boolean, it will return true
    if (is_bool($a) === true) {
        echo "Yes, this is a boolean";
    }

    // Since $b is not a boolean, it will return false
    if (is_bool($b) === false) {
        echo "No, this is not a boolean";
    }

    echo "<br>";

    class myClass{
        public $x;
    }

    $obj = new myClass();

    $myValue = $obj;
    var_dump(boolval($myValue));
?>