<?php
    $string = "His name is J'ohn Kerry";
    echo addslashes($string);

    echo "<br>";

    $like = "audi,bmw,chevrolet,ducati,ferrari";
    $type = explode(",", $like);
    echo "<pre>";
    print_r($type);
    echo "</pre>";

    $myArr = array("HTML", "CSS", "Javascript", "Jquery", "PHP", "ASP.net");
    $imp = implode('","', $myArr);

    $myStr =  '"'.$imp.'" '."<br>";
    echo $myStr;

    $mytutorial = '<br> means line break.';

    echo htmlentities($mytutorial);

    $username = " durjoy ";
    echo $username."<br>";
    echo trim($username)."<br>";


    $myStr = "Line1\nLine2\nLine3\n";

    $myStr = nl2br($myStr);

    echo $myStr;


//str_pad(string,length,pad_string,pad_type)

    $myStr = "Hello World!";

    echo str_pad($myStr, 100, $myStr)."<br>";

    echo str_repeat($myStr, 5)."<br>";

    $myStr = "BASIS Institute of Technology & Management";

    echo str_replace("O", "e", $myStr)."<br>";

    $myArr = str_split($myStr, 5);
    echo "<pre>";
    print_r($myArr);
    echo "</pre>";

    echo "<br>";


//substr_compare(string1,string2,startpos,length,case)

    $mainStr = "Hello World! How is life? Hey you, how are you?";
    $subStr = "How is";

    echo substr_compare($mainStr, $subStr, 13)."<br>";

    echo substr_replace($mainStr, "How are you doing?", 0)."<br>";


    $mainStr = "hello World! how is life? hey you, how are you?";
    echo ucfirst($mainStr)."<br>";
    echo ucwords($mainStr);

?>